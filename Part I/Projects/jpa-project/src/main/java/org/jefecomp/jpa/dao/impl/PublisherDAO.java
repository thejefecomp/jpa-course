/**
 * 
 */
package org.jefecomp.jpa.dao.impl;

import org.jefecomp.jpa.entities.Publisher;

/**
 * @author Jeferson Souza (thejefecomp)
 *
 */
public class PublisherDAO extends GenericDAOImpl<Publisher> {

}
