/**
 * 
 */
package org.jefecomp.jpa.dao.impl;

import org.jefecomp.jpa.entities.Person;

/**
 * @author Jeferson Souza (thejefecomp)
 *
 */
public class PersonDAO extends GenericDAOImpl<Person> {

}
