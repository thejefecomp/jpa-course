/**
 * 
 */
package org.jefecomp.jpa.dao.impl;

import org.jefecomp.jpa.entities.Author;

/**
 * @author Jeferson Souza (thejefecomp)
 *
 */
public class AuthorDAO extends GenericDAOImpl<Author> {

}
