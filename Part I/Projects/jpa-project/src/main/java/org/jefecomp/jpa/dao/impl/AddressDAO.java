/**
 * 
 */
package org.jefecomp.jpa.dao.impl;

import org.jefecomp.jpa.entities.Address;

/**
 * @author Jeferson Souza (thejefecomp)
 *
 */
public class AddressDAO extends GenericDAOImpl<Address> {

}
