/**
 * 
 */
package org.jefecomp.jpa.dao.impl;

import org.jefecomp.jpa.entities.Book;

/**
 * @author Jeferson Souza (thejefecomp)
 *
 */
public class BookDAO extends GenericDAOImpl<Book> {

}
